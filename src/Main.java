import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String fraseCompleta = "";
        List<String> ejemploIterable = new ArrayList<>();
        ejemploIterable.add ("Hola, ");
        ejemploIterable.add ("esto es una prueba ");
        ejemploIterable.add ("de que el ejemplo ");
        ejemploIterable.add ("de Iterable ");
        ejemploIterable.add ("funciona.");
        //forEach
        for (String elemento:ejemploIterable) {
            fraseCompleta+=elemento;
        };
        System.out.println(fraseCompleta);
    }
}
